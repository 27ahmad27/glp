$(function() {  
    var pull        = $('#pull');  
        menu        = $('nav ul');  
        menuHeight  = menu.height();  
  
    $(pull).on('click', function(e) {  
        e.preventDefault();  
        menu.slideToggle();  
    });

    $searching = false; 

    $(".search-icn").on('click',function(e){
    e.preventDefault();
    if( $searching == false ){
      $(".search").addClass('search-do');
      $(".search-icn").addClass('green-search');
      $searching = true;
    }else{
      $(".search").removeClass('search-do');
      $(".search-icn").removeClass('green-search');
      $searching = false;
    }
  }); 

    var vid = document.getElementById("bgvid");
	function vidFade() {
		vid.classList.add("stopfade");
	}
	vid.addEventListener('ended', function() {
	// only functional if "loop" is removed
	vid.pause();
	// to capture IE10
	vidFade();
	});
	vid.addEventListener("click", function() {
	vid.classList.toggle("stopfade");
	if (vid.paused) {
	vid.play();
	vid.innerHTML = "Pause";
	} else {
	vid.pause();
	vid.innerHTML = "Paused";
	}
	});

	$('.videos-items').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3
	});
	$(".change-video").on('click',function(e){
		e.preventDefault();
		console.log('clicked');
		videourl = $(this).data('video');
		vid = document.getElementById("bgvid");
		vid.src = videourl;
		vid.load();
		vid.play();
	});

	window.addEventListener('load', function() {
    var video = document.querySelector('#bgvid');
    var preloader = document.querySelector('.loader');

    function checkLoad() {
        if (video.readyState === 4) {
            preloader.parentNode.removeChild(preloader);
        } else {
            setTimeout(checkLoad, 100);
        }
    }

    checkLoad();
}, false);

    $(".nano").nanoScroller();

});

$(window).resize(function(){  
    var w = $(window).width();  
    if(w > 320 && menu.is(':hidden')) {  
        menu.removeAttr('style');  
    }  
});
